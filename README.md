# Crap Manager

An app to manage a wide array of tasks and hobbies and to do some statistics on them.

## Database

A graph representing the database structure:

![](database_graph.svg)

To make the graph run

```bash
dot database_graph.dot -Tsvg > database_graph.svg
```

## Ideas

- [ ] Image color picker (courtesy of Giuglio)
