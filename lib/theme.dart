import 'package:flutter/material.dart';

final green = Colors.green.shade600;
const white = Color.fromARGB(255, 225, 225, 225);
const purple = Color.fromARGB(255, 69, 39, 82);

final buttonStyle = ButtonStyle(
  shape: MaterialStatePropertyAll(RoundedRectangleBorder(
    borderRadius: BorderRadius.circular(30.0),
  )),
  backgroundColor: const MaterialStatePropertyAll(purple),
  foregroundColor: const MaterialStatePropertyAll(white),
);

final theme = ThemeData(
  colorScheme: ColorScheme.dark(
    primary: green,
    onPrimary: white,
    secondary: purple,
    onSecondary: white,
  ),
  filledButtonTheme: FilledButtonThemeData(style: buttonStyle),
  elevatedButtonTheme: ElevatedButtonThemeData(style: buttonStyle),
  floatingActionButtonTheme: FloatingActionButtonThemeData(
    backgroundColor: green,
    foregroundColor: white,
  ),
);
