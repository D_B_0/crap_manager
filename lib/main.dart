import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:calendar_view/calendar_view.dart';

import 'package:crap_manager/theme.dart';
import 'package:crap_manager/pages/home.dart';

Future<void> main() async {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return CalendarControllerProvider(
      controller: EventController(),
      child: MaterialApp(
        localizationsDelegates: AppLocalizations.localizationsDelegates,
        supportedLocales: AppLocalizations.supportedLocales,
        // used for testing
        // localeResolutionCallback: (locale, supportedLocales) {
        //   return supportedLocales.last;
        // },
        theme: theme,
        home: const HomePage(),
      ),
    );
  }
}
