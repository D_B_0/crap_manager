import 'package:crap_manager/string_extension.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'package:crap_manager/db/helper_types.dart';
import 'package:crap_manager/db/tables.dart';
import 'package:crap_manager/db/definition.dart';

class NewKindPage extends StatefulWidget {
  const NewKindPage({super.key});

  @override
  State<StatefulWidget> createState() => _NewKindPageState();
}

class _NewKindPageState extends State<NewKindPage> {
  _NewKindPageState();

  String? _name;

  final List<KindEntryCreateDataNullable> _entries = [];
  final List<int> _entriesKeys = [];
  int _entriesKeyLast = 0;

  final _formKey = GlobalKey<FormState>();

  Widget _kindNameInput() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
      child: TextFormField(
        validator: (value) {
          if (value == null || value.isEmpty) {
            return AppLocalizations.of(context)!.labelNameNotEmpty;
          }
          return null;
        },
        onChanged: (value) {
          setState(() => _name = value);
        },
        decoration: InputDecoration(
          hintText: AppLocalizations.of(context)!.labelName,
          border: const UnderlineInputBorder(),
        ),
      ),
    );
  }

  Widget _entryTypeSelector(int index) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
      child: DropdownButtonFormField(
          value: _entries[index].type,
          items: [
            DropdownMenuItem(
                value: null,
                child: Text(AppLocalizations.of(context)!.chooseType)),
            ...Types.values
                .map<DropdownMenuItem<Types?>>((type) => DropdownMenuItem(
                    value: type,
                    child: Text(
                        AppLocalizations.of(context)!.typesEnum(type.name))))
                .toList()
          ],
          validator: (type) {
            if (type == null) {
              return AppLocalizations.of(context)!.chooseType;
            }
            return null;
          },
          onChanged: (type) {
            setState(() => _entries[index].type = type!);
          }),
    );
  }

  Widget _entryNameSelector(int index) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
      child: TextFormField(
        initialValue: _entries[index].name,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return AppLocalizations.of(context)!.entryNameNotEmpty;
          }
          return null;
        },
        onChanged: (value) {
          setState(() => _entries[index].name = value);
        },
        decoration: InputDecoration(
          hintText: AppLocalizations.of(context)!.entryName,
          border: const UnderlineInputBorder(),
        ),
      ),
    );
  }

  Widget _entriesList() {
    return ListView.builder(
      itemCount: _entries.length,
      itemBuilder: (context, index) {
        return Dismissible(
            key: Key(_entriesKeys[index].toString()),
            onDismissed: (direction) => setState(() {
                  _entries.removeAt(index);
                  _entriesKeys.removeAt(index);
                }),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                  child: _entryTypeSelector(index),
                ),
                Expanded(
                  child: _entryNameSelector(index),
                ),
              ],
            ));
      },
    );
  }

  Widget _addEntryButton() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      child: FilledButton(
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(AppLocalizations.of(context)!.addEntry),
            const Icon(Icons.add),
          ],
        ),
        onPressed: () {
          setState(() {
            _entries.add(KindEntryCreateDataNullable(null, null));
            _entriesKeys.add(_entriesKeyLast);
            _entriesKeyLast++;
          });
        },
      ),
    );
  }

  Form _buildForm() {
    return Form(
        key: _formKey,
        child: Center(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                _kindNameInput(),
                Flexible(
                  child: _entriesList(),
                ),
                _addEntryButton(),
              ]),
        ));
  }

  void _submit() {
    if (!_formKey.currentState!.validate()) {
      return;
    }
    db.insertKindWithEntries(_name!,
        _entries.map((e) => KindEntryCreateData(e.name!, e.type!)).toList());
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context)!.newLabel),
      ),
      body: _buildForm(),
      floatingActionButton: FloatingActionButton(
        onPressed: _submit,
        tooltip: AppLocalizations.of(context)!.submit,
        child: const Icon(Icons.arrow_forward),
      ),
    );
  }
}
