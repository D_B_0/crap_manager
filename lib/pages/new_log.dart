import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'package:crap_manager/db/definition.dart';
import 'package:crap_manager/db/tables.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';

class NewLogPage extends StatefulWidget {
  const NewLogPage({super.key});

  @override
  State<StatefulWidget> createState() => _NewLogPageState();
}

class _NewLogPageState extends State<NewLogPage> {
  _NewLogPageState();

  List<Kind> _kindList = [];
  List<KindEntry> _entriesList = [];
  final Map<KindEntry, dynamic> _entryData = {};

  DateTime _selectedDate = DateTime.now();
  TimeOfDay _selectedTime = TimeOfDay.now();
  Kind? _selectedKind;

  final Map<KindEntry, TextEditingController> _colorSelectorController = {};

  final _formKey = GlobalKey<FormState>();

  Future<void> _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: _selectedDate,
        firstDate: DateTime(2000),
        lastDate: DateTime(2100));
    if (picked != null && picked != _selectedDate) {
      setState(() {
        _selectedDate = picked;
      });
    }
  }

  Future<void> _selectTime(BuildContext context) async {
    final TimeOfDay? picked =
        await showTimePicker(context: context, initialTime: _selectedTime);
    if (picked != null && picked != _selectedTime) {
      setState(() {
        _selectedTime = picked;
      });
    }
  }

  @override
  void initState() {
    var kindListFuture = db.select(db.kinds).get();
    super.initState();
    kindListFuture.then((value) => setState(() => _kindList = value));
  }

  DateTime _selectedTimeDate() {
    return DateTime(0, 0, 0, _selectedTime.hour, _selectedTime.minute);
  }

  Widget _dateAndTimeSelector() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          ElevatedButton(
              onPressed: () => _selectDate(context),
              child: Text(AppLocalizations.of(context)!.date(_selectedDate))),
          ElevatedButton(
              onPressed: () => _selectTime(context),
              child: Text(
                  AppLocalizations.of(context)!.time(_selectedTimeDate()))),
        ],
      ),
    );
  }

  Widget _kindSelector() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
      child: DropdownButtonFormField(
        decoration: InputDecoration(
          hintText: AppLocalizations.of(context)!.selectLabel,
          border: const UnderlineInputBorder(),
        ),
        validator: (value) {
          if (value == null) {
            return AppLocalizations.of(context)!.selectLabel;
          }
          return null;
        },
        value: _selectedKind,
        items: _kindList
            .map<DropdownMenuItem<Kind>>(
                (kind) => DropdownMenuItem(value: kind, child: Text(kind.name)))
            .toList(),
        onChanged: (kind) {
          setState(() => _selectedKind = kind);
          _updateEntriesList();
        },
      ),
    );
  }

  Widget _entryNumberInput(KindEntry entry) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
      child: TextFormField(
        decoration: InputDecoration(
          hintText: entry.name,
          border: const UnderlineInputBorder(),
        ),
        validator: (value) {
          if (value == null || value.isEmpty) {
            return AppLocalizations.of(context)!.insertNumber;
          }
          try {
            double.parse(value);
            return null;
          } on FormatException {
            return AppLocalizations.of(context)!.invalidNumber;
          }
        },
        keyboardType: TextInputType.number,
        inputFormatters: <TextInputFormatter>[
          // FilteringTextInputFormatter.digitsOnly
          TextInputFormatter.withFunction((oldValue, newValue) {
            if (newValue.text.isEmpty) {
              return newValue;
            }
            if (RegExp("^[0-9]+(?:\\.[0-9]*)?\$").stringMatch(newValue.text) !=
                null) {
              return newValue;
            }
            return oldValue;
          })
        ],
        onChanged: (value) => setState(() {
          try {
            _entryData[entry] = double.parse(value);
          } on FormatException {
            // ignore, this shouldn't be an issue because of the validation
          }
        }),
      ),
    );
  }

  Widget _entryTextInput(KindEntry entry) {
    setState(() {
      if (_entryData[entry] == null) _entryData[entry] = "";
    });
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
      child: TextFormField(
        decoration: InputDecoration(
          hintText: entry.name,
          border: const UnderlineInputBorder(),
        ),
        onChanged: (value) => setState(() => _entryData[entry] = value),
      ),
    );
  }

  void _showColorPicker(KindEntry entry) {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(AppLocalizations.of(context)!.pickColor),
        content: SingleChildScrollView(
          child: ColorPicker(
            onColorChanged: (value) {
              setState(() => _entryData[entry] = value);
              final hex = value.value & 0x00ffffff;
              _colorSelectorController[entry]!.text =
                  "0x${hex.toRadixString(16).toUpperCase()}";
            },
            pickerColor: _entryData[entry] ?? Colors.white,
            enableAlpha: false,
          ),
        ),
        actions: [
          ElevatedButton(
            child: Text(AppLocalizations.of(context)!.done),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }

  Widget _entryColorInput(KindEntry entry) {
    if (_colorSelectorController[entry] == null) {
      _colorSelectorController[entry] = TextEditingController();
    }
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(0, 0, 20, 0),
                  child: TextFormField(
                    controller: _colorSelectorController[entry],
                    decoration: InputDecoration(
                      hintText: entry.name,
                      border: const UnderlineInputBorder(),
                    ),
                    inputFormatters: [
                      TextInputFormatter.withFunction((oldValue, newValue) {
                        return oldValue;
                      })
                    ],
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return AppLocalizations.of(context)!
                            .cantLeaveEntryEmpty(entry.name);
                      }
                      return null;
                    },
                    // enabled: false,
                  ),
                ),
              ),
              ElevatedButton(
                onPressed: () => _showColorPicker(entry),
                style: ButtonStyle(
                  backgroundColor: MaterialStatePropertyAll(_entryData[entry]),
                ),
                child: Text(_entryData[entry] == null
                    ? AppLocalizations.of(context)!.pickColor
                    : ""),
              )
            ],
          ),
        ],
      ),
    );
  }

  Widget _entryInput(KindEntry entry) {
    switch (entry.type) {
      case Types.number:
        return _entryNumberInput(entry);
      case Types.text:
        return _entryTextInput(entry);
      case Types.color:
        return _entryColorInput(entry);
      default:
        throw UnimplementedError(
            "Hey fuckhead, did you add a type? You should check for it!");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context)!.newLog),
      ),
      body: Center(
        child: Form(
            key: _formKey,
            child:
                Column(mainAxisAlignment: MainAxisAlignment.start, children: [
              _dateAndTimeSelector(),
              _kindSelector(),
              Flexible(
                child: ListView(
                  children: [
                    for (final entry in _entriesList) _entryInput(entry),
                  ],
                ),
              ),
            ])),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _submit,
        tooltip: AppLocalizations.of(context)!.submit,
        child: const Icon(Icons.arrow_forward),
      ),
    );
  }

  void _submit() {
    if (!_formKey.currentState!.validate()) {
      return;
    }
    final createdAt = DateTime(_selectedDate.year, _selectedDate.month,
        _selectedDate.day, _selectedTime.hour, _selectedTime.minute);

    db
        .insertLog(createdAt, _selectedKind!, _entryData)
        .then((value) => Navigator.pop(context))
        .catchError((err) {
      debugPrint(
          "The insertion failed! We should tell the user (maybe alert?)");
    });
  }

  Future _updateEntriesList() async {
    if (_selectedKind == null) return;
    final entries =
        (await db.entriesWithKind(_selectedKind!)).map((e) => e.entry).toList();
    setState(() {
      _entriesList = entries;
    });
  }
}
