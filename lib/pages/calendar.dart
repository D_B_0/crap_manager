import 'package:calendar_view/calendar_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'package:crap_manager/db/helper_types.dart';
import 'package:intl/intl.dart';

class CalendarPage extends StatelessWidget {
  const CalendarPage({super.key, required this.logsByKind});

  final Map<KindWithKindEntries, List<LogWithFullinfo>> logsByKind;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Scaffold(
      appBar: AppBar(title: Text(AppLocalizations.of(context)!.calendar)),
      body: MonthView(
        cellBuilder: (date, events, isToday, isInMonth) {
          return FilledCell(
            date: date,
            shouldHighlight: isToday,
            backgroundColor: isInMonth
                ? theme.colorScheme.background
                : theme.colorScheme.scrim,
            highlightColor: theme.colorScheme.primary,
            titleColor: isInMonth
                ? (isToday
                    ? theme.colorScheme.onPrimary
                    : theme.colorScheme.onBackground)
                : theme.colorScheme.onSurface,
            events: events,
          );
        },
        borderColor: theme.colorScheme.onBackground.withOpacity(0.4),
        weekDayStringBuilder: (p0) =>
            AppLocalizations.of(context)!.weekDay(p0.toString())[0],
        weekDayBuilder: (index) => WeekDayTile(
          weekDayStringBuilder: (p0) =>
              AppLocalizations.of(context)!.weekDay(p0.toString())[0],
          dayIndex: index,
          textStyle: theme.textTheme.titleSmall,
          backgroundColor: theme.colorScheme.secondary,
        ),
        headerStyle: HeaderStyle(
          headerTextStyle: theme.textTheme.titleLarge,
          decoration: BoxDecoration(color: theme.colorScheme.primary),
          leftIcon: Icon(
            Icons.chevron_left,
            size: 30,
            color: theme.textTheme.titleLarge?.color,
          ),
          rightIcon: Icon(
            Icons.chevron_right,
            size: 30,
            color: theme.textTheme.titleLarge?.color,
          ),
        ),
      ),
    );
  }
}
