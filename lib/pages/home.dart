import 'package:calendar_view/calendar_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'package:crap_manager/db/definition.dart';
import 'package:crap_manager/db/helper_types.dart';
import 'package:crap_manager/pages/calendar.dart';
import 'package:crap_manager/pages/new_kind.dart';
import 'package:crap_manager/pages/new_log.dart';
import 'package:crap_manager/widgets/kind_log_display.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Map<KindWithKindEntries, List<LogWithFullinfo>> _logsByKind = {};

  @override
  void initState() {
    super.initState();
    db.watchKindsAndLogs().listen((logs) {
      setState(() => _logsByKind = logs);

      // VERY inefficient, but idc right now
      final eventController = CalendarControllerProvider.of(context).controller;
      eventController.removeWhere((element) => true);
      for (final kind in logs.keys) {
        for (final logInfo in logs[kind]!) {
          final event = CalendarEventData(
            title: kind.kind.name,
            date: logInfo.log.createdAt,
            event: logInfo,
            startTime: logInfo.log.createdAt,
            endTime: logInfo.log.createdAt.add(const Duration(hours: 1)),
            color: Theme.of(context).colorScheme.primary,
          );
          eventController.add(event);
        }
      }
    });
  }

  Widget _newLabelButton() {
    return Container(
      padding: const EdgeInsets.fromLTRB(0, 8, 0, 0),
      child: ElevatedButton(
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(AppLocalizations.of(context)!.newLabel),
            const Icon(Icons.add),
          ],
        ),
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => const NewKindPage()));
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context)!.appName),
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            DrawerHeader(
              padding: const EdgeInsets.fromLTRB(16.0, 32.0, 16.0, 8.0),
              decoration: BoxDecoration(
                color: Theme.of(context).colorScheme.primary,
              ),
              child: Text(
                AppLocalizations.of(context)!.menuTitle,
                style: Theme.of(context).textTheme.headlineLarge,
              ),
            ),
            ListTile(
              title: Text(AppLocalizations.of(context)!.calendar),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => CalendarPage(
                      logsByKind: _logsByKind,
                    ),
                  ),
                );
              },
            ),
            ListTile(
              title: Text(AppLocalizations.of(context)!.wipeDbMenu),
              onTap: () {
                db.deleteEverything();
                Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Column(children: [
            for (final kind in _logsByKind.keys)
              KindLogDisplayWidget(kind, _logsByKind[kind]!),
            _newLabelButton()
          ]),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: ((context) => const NewLogPage())));
        },
        tooltip: AppLocalizations.of(context)!.newLog,
        child: const Icon(Icons.add),
      ),
    );
  }
}
