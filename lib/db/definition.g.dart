// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'definition.dart';

// ignore_for_file: type=lint
class $KindsTable extends Kinds with TableInfo<$KindsTable, Kind> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $KindsTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _nameMeta = const VerificationMeta('name');
  @override
  late final GeneratedColumn<String> name = GeneratedColumn<String>(
      'name', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  @override
  List<GeneratedColumn> get $columns => [id, name];
  @override
  String get aliasedName => _alias ?? 'kinds';
  @override
  String get actualTableName => 'kinds';
  @override
  VerificationContext validateIntegrity(Insertable<Kind> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name']!, _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Kind map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return Kind(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      name: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}name'])!,
    );
  }

  @override
  $KindsTable createAlias(String alias) {
    return $KindsTable(attachedDatabase, alias);
  }
}

class Kind extends DataClass implements Insertable<Kind> {
  final int id;
  final String name;
  const Kind({required this.id, required this.name});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['name'] = Variable<String>(name);
    return map;
  }

  KindsCompanion toCompanion(bool nullToAbsent) {
    return KindsCompanion(
      id: Value(id),
      name: Value(name),
    );
  }

  factory Kind.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return Kind(
      id: serializer.fromJson<int>(json['id']),
      name: serializer.fromJson<String>(json['name']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'name': serializer.toJson<String>(name),
    };
  }

  Kind copyWith({int? id, String? name}) => Kind(
        id: id ?? this.id,
        name: name ?? this.name,
      );
  @override
  String toString() {
    return (StringBuffer('Kind(')
          ..write('id: $id, ')
          ..write('name: $name')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, name);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is Kind && other.id == this.id && other.name == this.name);
}

class KindsCompanion extends UpdateCompanion<Kind> {
  final Value<int> id;
  final Value<String> name;
  const KindsCompanion({
    this.id = const Value.absent(),
    this.name = const Value.absent(),
  });
  KindsCompanion.insert({
    this.id = const Value.absent(),
    required String name,
  }) : name = Value(name);
  static Insertable<Kind> custom({
    Expression<int>? id,
    Expression<String>? name,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (name != null) 'name': name,
    });
  }

  KindsCompanion copyWith({Value<int>? id, Value<String>? name}) {
    return KindsCompanion(
      id: id ?? this.id,
      name: name ?? this.name,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('KindsCompanion(')
          ..write('id: $id, ')
          ..write('name: $name')
          ..write(')'))
        .toString();
  }
}

class $LogsTable extends Logs with TableInfo<$LogsTable, Log> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $LogsTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _createdAtMeta =
      const VerificationMeta('createdAt');
  @override
  late final GeneratedColumn<DateTime> createdAt = GeneratedColumn<DateTime>(
      'created_at', aliasedName, false,
      type: DriftSqlType.dateTime,
      requiredDuringInsert: false,
      defaultValue: currentDateAndTime);
  static const VerificationMeta _kindMeta = const VerificationMeta('kind');
  @override
  late final GeneratedColumn<int> kind = GeneratedColumn<int>(
      'kind', aliasedName, false,
      type: DriftSqlType.int,
      requiredDuringInsert: true,
      defaultConstraints: GeneratedColumn.constraintIsAlways(
          'REFERENCES kinds (id) ON DELETE CASCADE'));
  @override
  List<GeneratedColumn> get $columns => [id, createdAt, kind];
  @override
  String get aliasedName => _alias ?? 'logs';
  @override
  String get actualTableName => 'logs';
  @override
  VerificationContext validateIntegrity(Insertable<Log> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('created_at')) {
      context.handle(_createdAtMeta,
          createdAt.isAcceptableOrUnknown(data['created_at']!, _createdAtMeta));
    }
    if (data.containsKey('kind')) {
      context.handle(
          _kindMeta, kind.isAcceptableOrUnknown(data['kind']!, _kindMeta));
    } else if (isInserting) {
      context.missing(_kindMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Log map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return Log(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      createdAt: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}created_at'])!,
      kind: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}kind'])!,
    );
  }

  @override
  $LogsTable createAlias(String alias) {
    return $LogsTable(attachedDatabase, alias);
  }
}

class Log extends DataClass implements Insertable<Log> {
  final int id;
  final DateTime createdAt;
  final int kind;
  const Log({required this.id, required this.createdAt, required this.kind});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['created_at'] = Variable<DateTime>(createdAt);
    map['kind'] = Variable<int>(kind);
    return map;
  }

  LogsCompanion toCompanion(bool nullToAbsent) {
    return LogsCompanion(
      id: Value(id),
      createdAt: Value(createdAt),
      kind: Value(kind),
    );
  }

  factory Log.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return Log(
      id: serializer.fromJson<int>(json['id']),
      createdAt: serializer.fromJson<DateTime>(json['createdAt']),
      kind: serializer.fromJson<int>(json['kind']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'createdAt': serializer.toJson<DateTime>(createdAt),
      'kind': serializer.toJson<int>(kind),
    };
  }

  Log copyWith({int? id, DateTime? createdAt, int? kind}) => Log(
        id: id ?? this.id,
        createdAt: createdAt ?? this.createdAt,
        kind: kind ?? this.kind,
      );
  @override
  String toString() {
    return (StringBuffer('Log(')
          ..write('id: $id, ')
          ..write('createdAt: $createdAt, ')
          ..write('kind: $kind')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, createdAt, kind);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is Log &&
          other.id == this.id &&
          other.createdAt == this.createdAt &&
          other.kind == this.kind);
}

class LogsCompanion extends UpdateCompanion<Log> {
  final Value<int> id;
  final Value<DateTime> createdAt;
  final Value<int> kind;
  const LogsCompanion({
    this.id = const Value.absent(),
    this.createdAt = const Value.absent(),
    this.kind = const Value.absent(),
  });
  LogsCompanion.insert({
    this.id = const Value.absent(),
    this.createdAt = const Value.absent(),
    required int kind,
  }) : kind = Value(kind);
  static Insertable<Log> custom({
    Expression<int>? id,
    Expression<DateTime>? createdAt,
    Expression<int>? kind,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (createdAt != null) 'created_at': createdAt,
      if (kind != null) 'kind': kind,
    });
  }

  LogsCompanion copyWith(
      {Value<int>? id, Value<DateTime>? createdAt, Value<int>? kind}) {
    return LogsCompanion(
      id: id ?? this.id,
      createdAt: createdAt ?? this.createdAt,
      kind: kind ?? this.kind,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (createdAt.present) {
      map['created_at'] = Variable<DateTime>(createdAt.value);
    }
    if (kind.present) {
      map['kind'] = Variable<int>(kind.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('LogsCompanion(')
          ..write('id: $id, ')
          ..write('createdAt: $createdAt, ')
          ..write('kind: $kind')
          ..write(')'))
        .toString();
  }
}

class $KindEntriesTable extends KindEntries
    with TableInfo<$KindEntriesTable, KindEntry> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $KindEntriesTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _kindMeta = const VerificationMeta('kind');
  @override
  late final GeneratedColumn<int> kind = GeneratedColumn<int>(
      'kind', aliasedName, false,
      type: DriftSqlType.int,
      requiredDuringInsert: true,
      defaultConstraints: GeneratedColumn.constraintIsAlways(
          'REFERENCES kinds (id) ON DELETE CASCADE'));
  static const VerificationMeta _typeMeta = const VerificationMeta('type');
  @override
  late final GeneratedColumnWithTypeConverter<Types, int> type =
      GeneratedColumn<int>('type', aliasedName, false,
              type: DriftSqlType.int, requiredDuringInsert: true)
          .withConverter<Types>($KindEntriesTable.$convertertype);
  static const VerificationMeta _nameMeta = const VerificationMeta('name');
  @override
  late final GeneratedColumn<String> name = GeneratedColumn<String>(
      'name', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  @override
  List<GeneratedColumn> get $columns => [id, kind, type, name];
  @override
  String get aliasedName => _alias ?? 'kind_entries';
  @override
  String get actualTableName => 'kind_entries';
  @override
  VerificationContext validateIntegrity(Insertable<KindEntry> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('kind')) {
      context.handle(
          _kindMeta, kind.isAcceptableOrUnknown(data['kind']!, _kindMeta));
    } else if (isInserting) {
      context.missing(_kindMeta);
    }
    context.handle(_typeMeta, const VerificationResult.success());
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name']!, _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  KindEntry map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return KindEntry(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      kind: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}kind'])!,
      type: $KindEntriesTable.$convertertype.fromSql(attachedDatabase
          .typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}type'])!),
      name: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}name'])!,
    );
  }

  @override
  $KindEntriesTable createAlias(String alias) {
    return $KindEntriesTable(attachedDatabase, alias);
  }

  static JsonTypeConverter2<Types, int, int> $convertertype =
      const EnumIndexConverter<Types>(Types.values);
}

class KindEntry extends DataClass implements Insertable<KindEntry> {
  final int id;
  final int kind;
  final Types type;
  final String name;
  const KindEntry(
      {required this.id,
      required this.kind,
      required this.type,
      required this.name});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['kind'] = Variable<int>(kind);
    {
      final converter = $KindEntriesTable.$convertertype;
      map['type'] = Variable<int>(converter.toSql(type));
    }
    map['name'] = Variable<String>(name);
    return map;
  }

  KindEntriesCompanion toCompanion(bool nullToAbsent) {
    return KindEntriesCompanion(
      id: Value(id),
      kind: Value(kind),
      type: Value(type),
      name: Value(name),
    );
  }

  factory KindEntry.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return KindEntry(
      id: serializer.fromJson<int>(json['id']),
      kind: serializer.fromJson<int>(json['kind']),
      type: $KindEntriesTable.$convertertype
          .fromJson(serializer.fromJson<int>(json['type'])),
      name: serializer.fromJson<String>(json['name']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'kind': serializer.toJson<int>(kind),
      'type':
          serializer.toJson<int>($KindEntriesTable.$convertertype.toJson(type)),
      'name': serializer.toJson<String>(name),
    };
  }

  KindEntry copyWith({int? id, int? kind, Types? type, String? name}) =>
      KindEntry(
        id: id ?? this.id,
        kind: kind ?? this.kind,
        type: type ?? this.type,
        name: name ?? this.name,
      );
  @override
  String toString() {
    return (StringBuffer('KindEntry(')
          ..write('id: $id, ')
          ..write('kind: $kind, ')
          ..write('type: $type, ')
          ..write('name: $name')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, kind, type, name);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is KindEntry &&
          other.id == this.id &&
          other.kind == this.kind &&
          other.type == this.type &&
          other.name == this.name);
}

class KindEntriesCompanion extends UpdateCompanion<KindEntry> {
  final Value<int> id;
  final Value<int> kind;
  final Value<Types> type;
  final Value<String> name;
  const KindEntriesCompanion({
    this.id = const Value.absent(),
    this.kind = const Value.absent(),
    this.type = const Value.absent(),
    this.name = const Value.absent(),
  });
  KindEntriesCompanion.insert({
    this.id = const Value.absent(),
    required int kind,
    required Types type,
    required String name,
  })  : kind = Value(kind),
        type = Value(type),
        name = Value(name);
  static Insertable<KindEntry> custom({
    Expression<int>? id,
    Expression<int>? kind,
    Expression<int>? type,
    Expression<String>? name,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (kind != null) 'kind': kind,
      if (type != null) 'type': type,
      if (name != null) 'name': name,
    });
  }

  KindEntriesCompanion copyWith(
      {Value<int>? id,
      Value<int>? kind,
      Value<Types>? type,
      Value<String>? name}) {
    return KindEntriesCompanion(
      id: id ?? this.id,
      kind: kind ?? this.kind,
      type: type ?? this.type,
      name: name ?? this.name,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (kind.present) {
      map['kind'] = Variable<int>(kind.value);
    }
    if (type.present) {
      final converter = $KindEntriesTable.$convertertype;
      map['type'] = Variable<int>(converter.toSql(type.value));
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('KindEntriesCompanion(')
          ..write('id: $id, ')
          ..write('kind: $kind, ')
          ..write('type: $type, ')
          ..write('name: $name')
          ..write(')'))
        .toString();
  }
}

class $NumEntriesTable extends NumEntries
    with TableInfo<$NumEntriesTable, NumEntry> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $NumEntriesTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _entryMeta = const VerificationMeta('entry');
  @override
  late final GeneratedColumn<double> entry = GeneratedColumn<double>(
      'entry', aliasedName, false,
      type: DriftSqlType.double, requiredDuringInsert: true);
  static const VerificationMeta _kindEntryMeta =
      const VerificationMeta('kindEntry');
  @override
  late final GeneratedColumn<int> kindEntry = GeneratedColumn<int>(
      'kind_entry', aliasedName, false,
      type: DriftSqlType.int,
      requiredDuringInsert: true,
      defaultConstraints: GeneratedColumn.constraintIsAlways(
          'REFERENCES kind_entries (id) ON DELETE CASCADE'));
  static const VerificationMeta _logMeta = const VerificationMeta('log');
  @override
  late final GeneratedColumn<int> log = GeneratedColumn<int>(
      'log', aliasedName, false,
      type: DriftSqlType.int,
      requiredDuringInsert: true,
      defaultConstraints: GeneratedColumn.constraintIsAlways(
          'REFERENCES logs (id) ON DELETE CASCADE'));
  @override
  List<GeneratedColumn> get $columns => [id, entry, kindEntry, log];
  @override
  String get aliasedName => _alias ?? 'num_entries';
  @override
  String get actualTableName => 'num_entries';
  @override
  VerificationContext validateIntegrity(Insertable<NumEntry> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('entry')) {
      context.handle(
          _entryMeta, entry.isAcceptableOrUnknown(data['entry']!, _entryMeta));
    } else if (isInserting) {
      context.missing(_entryMeta);
    }
    if (data.containsKey('kind_entry')) {
      context.handle(_kindEntryMeta,
          kindEntry.isAcceptableOrUnknown(data['kind_entry']!, _kindEntryMeta));
    } else if (isInserting) {
      context.missing(_kindEntryMeta);
    }
    if (data.containsKey('log')) {
      context.handle(
          _logMeta, log.isAcceptableOrUnknown(data['log']!, _logMeta));
    } else if (isInserting) {
      context.missing(_logMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  NumEntry map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return NumEntry(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      entry: attachedDatabase.typeMapping
          .read(DriftSqlType.double, data['${effectivePrefix}entry'])!,
      kindEntry: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}kind_entry'])!,
      log: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}log'])!,
    );
  }

  @override
  $NumEntriesTable createAlias(String alias) {
    return $NumEntriesTable(attachedDatabase, alias);
  }
}

class NumEntry extends DataClass implements Insertable<NumEntry> {
  final int id;
  final double entry;
  final int kindEntry;
  final int log;
  const NumEntry(
      {required this.id,
      required this.entry,
      required this.kindEntry,
      required this.log});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['entry'] = Variable<double>(entry);
    map['kind_entry'] = Variable<int>(kindEntry);
    map['log'] = Variable<int>(log);
    return map;
  }

  NumEntriesCompanion toCompanion(bool nullToAbsent) {
    return NumEntriesCompanion(
      id: Value(id),
      entry: Value(entry),
      kindEntry: Value(kindEntry),
      log: Value(log),
    );
  }

  factory NumEntry.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return NumEntry(
      id: serializer.fromJson<int>(json['id']),
      entry: serializer.fromJson<double>(json['entry']),
      kindEntry: serializer.fromJson<int>(json['kindEntry']),
      log: serializer.fromJson<int>(json['log']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'entry': serializer.toJson<double>(entry),
      'kindEntry': serializer.toJson<int>(kindEntry),
      'log': serializer.toJson<int>(log),
    };
  }

  NumEntry copyWith({int? id, double? entry, int? kindEntry, int? log}) =>
      NumEntry(
        id: id ?? this.id,
        entry: entry ?? this.entry,
        kindEntry: kindEntry ?? this.kindEntry,
        log: log ?? this.log,
      );
  @override
  String toString() {
    return (StringBuffer('NumEntry(')
          ..write('id: $id, ')
          ..write('entry: $entry, ')
          ..write('kindEntry: $kindEntry, ')
          ..write('log: $log')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, entry, kindEntry, log);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is NumEntry &&
          other.id == this.id &&
          other.entry == this.entry &&
          other.kindEntry == this.kindEntry &&
          other.log == this.log);
}

class NumEntriesCompanion extends UpdateCompanion<NumEntry> {
  final Value<int> id;
  final Value<double> entry;
  final Value<int> kindEntry;
  final Value<int> log;
  const NumEntriesCompanion({
    this.id = const Value.absent(),
    this.entry = const Value.absent(),
    this.kindEntry = const Value.absent(),
    this.log = const Value.absent(),
  });
  NumEntriesCompanion.insert({
    this.id = const Value.absent(),
    required double entry,
    required int kindEntry,
    required int log,
  })  : entry = Value(entry),
        kindEntry = Value(kindEntry),
        log = Value(log);
  static Insertable<NumEntry> custom({
    Expression<int>? id,
    Expression<double>? entry,
    Expression<int>? kindEntry,
    Expression<int>? log,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (entry != null) 'entry': entry,
      if (kindEntry != null) 'kind_entry': kindEntry,
      if (log != null) 'log': log,
    });
  }

  NumEntriesCompanion copyWith(
      {Value<int>? id,
      Value<double>? entry,
      Value<int>? kindEntry,
      Value<int>? log}) {
    return NumEntriesCompanion(
      id: id ?? this.id,
      entry: entry ?? this.entry,
      kindEntry: kindEntry ?? this.kindEntry,
      log: log ?? this.log,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (entry.present) {
      map['entry'] = Variable<double>(entry.value);
    }
    if (kindEntry.present) {
      map['kind_entry'] = Variable<int>(kindEntry.value);
    }
    if (log.present) {
      map['log'] = Variable<int>(log.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('NumEntriesCompanion(')
          ..write('id: $id, ')
          ..write('entry: $entry, ')
          ..write('kindEntry: $kindEntry, ')
          ..write('log: $log')
          ..write(')'))
        .toString();
  }
}

class $TextEntriesTable extends TextEntries
    with TableInfo<$TextEntriesTable, TextEntry> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $TextEntriesTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _entryMeta = const VerificationMeta('entry');
  @override
  late final GeneratedColumn<String> entry = GeneratedColumn<String>(
      'entry', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _kindEntryMeta =
      const VerificationMeta('kindEntry');
  @override
  late final GeneratedColumn<int> kindEntry = GeneratedColumn<int>(
      'kind_entry', aliasedName, false,
      type: DriftSqlType.int,
      requiredDuringInsert: true,
      defaultConstraints: GeneratedColumn.constraintIsAlways(
          'REFERENCES kind_entries (id) ON DELETE CASCADE'));
  static const VerificationMeta _logMeta = const VerificationMeta('log');
  @override
  late final GeneratedColumn<int> log = GeneratedColumn<int>(
      'log', aliasedName, false,
      type: DriftSqlType.int,
      requiredDuringInsert: true,
      defaultConstraints: GeneratedColumn.constraintIsAlways(
          'REFERENCES logs (id) ON DELETE CASCADE'));
  @override
  List<GeneratedColumn> get $columns => [id, entry, kindEntry, log];
  @override
  String get aliasedName => _alias ?? 'text_entries';
  @override
  String get actualTableName => 'text_entries';
  @override
  VerificationContext validateIntegrity(Insertable<TextEntry> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('entry')) {
      context.handle(
          _entryMeta, entry.isAcceptableOrUnknown(data['entry']!, _entryMeta));
    } else if (isInserting) {
      context.missing(_entryMeta);
    }
    if (data.containsKey('kind_entry')) {
      context.handle(_kindEntryMeta,
          kindEntry.isAcceptableOrUnknown(data['kind_entry']!, _kindEntryMeta));
    } else if (isInserting) {
      context.missing(_kindEntryMeta);
    }
    if (data.containsKey('log')) {
      context.handle(
          _logMeta, log.isAcceptableOrUnknown(data['log']!, _logMeta));
    } else if (isInserting) {
      context.missing(_logMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  TextEntry map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return TextEntry(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      entry: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}entry'])!,
      kindEntry: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}kind_entry'])!,
      log: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}log'])!,
    );
  }

  @override
  $TextEntriesTable createAlias(String alias) {
    return $TextEntriesTable(attachedDatabase, alias);
  }
}

class TextEntry extends DataClass implements Insertable<TextEntry> {
  final int id;
  final String entry;
  final int kindEntry;
  final int log;
  const TextEntry(
      {required this.id,
      required this.entry,
      required this.kindEntry,
      required this.log});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['entry'] = Variable<String>(entry);
    map['kind_entry'] = Variable<int>(kindEntry);
    map['log'] = Variable<int>(log);
    return map;
  }

  TextEntriesCompanion toCompanion(bool nullToAbsent) {
    return TextEntriesCompanion(
      id: Value(id),
      entry: Value(entry),
      kindEntry: Value(kindEntry),
      log: Value(log),
    );
  }

  factory TextEntry.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return TextEntry(
      id: serializer.fromJson<int>(json['id']),
      entry: serializer.fromJson<String>(json['entry']),
      kindEntry: serializer.fromJson<int>(json['kindEntry']),
      log: serializer.fromJson<int>(json['log']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'entry': serializer.toJson<String>(entry),
      'kindEntry': serializer.toJson<int>(kindEntry),
      'log': serializer.toJson<int>(log),
    };
  }

  TextEntry copyWith({int? id, String? entry, int? kindEntry, int? log}) =>
      TextEntry(
        id: id ?? this.id,
        entry: entry ?? this.entry,
        kindEntry: kindEntry ?? this.kindEntry,
        log: log ?? this.log,
      );
  @override
  String toString() {
    return (StringBuffer('TextEntry(')
          ..write('id: $id, ')
          ..write('entry: $entry, ')
          ..write('kindEntry: $kindEntry, ')
          ..write('log: $log')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, entry, kindEntry, log);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is TextEntry &&
          other.id == this.id &&
          other.entry == this.entry &&
          other.kindEntry == this.kindEntry &&
          other.log == this.log);
}

class TextEntriesCompanion extends UpdateCompanion<TextEntry> {
  final Value<int> id;
  final Value<String> entry;
  final Value<int> kindEntry;
  final Value<int> log;
  const TextEntriesCompanion({
    this.id = const Value.absent(),
    this.entry = const Value.absent(),
    this.kindEntry = const Value.absent(),
    this.log = const Value.absent(),
  });
  TextEntriesCompanion.insert({
    this.id = const Value.absent(),
    required String entry,
    required int kindEntry,
    required int log,
  })  : entry = Value(entry),
        kindEntry = Value(kindEntry),
        log = Value(log);
  static Insertable<TextEntry> custom({
    Expression<int>? id,
    Expression<String>? entry,
    Expression<int>? kindEntry,
    Expression<int>? log,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (entry != null) 'entry': entry,
      if (kindEntry != null) 'kind_entry': kindEntry,
      if (log != null) 'log': log,
    });
  }

  TextEntriesCompanion copyWith(
      {Value<int>? id,
      Value<String>? entry,
      Value<int>? kindEntry,
      Value<int>? log}) {
    return TextEntriesCompanion(
      id: id ?? this.id,
      entry: entry ?? this.entry,
      kindEntry: kindEntry ?? this.kindEntry,
      log: log ?? this.log,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (entry.present) {
      map['entry'] = Variable<String>(entry.value);
    }
    if (kindEntry.present) {
      map['kind_entry'] = Variable<int>(kindEntry.value);
    }
    if (log.present) {
      map['log'] = Variable<int>(log.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('TextEntriesCompanion(')
          ..write('id: $id, ')
          ..write('entry: $entry, ')
          ..write('kindEntry: $kindEntry, ')
          ..write('log: $log')
          ..write(')'))
        .toString();
  }
}

class $ColorEntriesTable extends ColorEntries
    with TableInfo<$ColorEntriesTable, ColorEntry> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $ColorEntriesTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _entryMeta = const VerificationMeta('entry');
  @override
  late final GeneratedColumn<int> entry = GeneratedColumn<int>(
      'entry', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  static const VerificationMeta _kindEntryMeta =
      const VerificationMeta('kindEntry');
  @override
  late final GeneratedColumn<int> kindEntry = GeneratedColumn<int>(
      'kind_entry', aliasedName, false,
      type: DriftSqlType.int,
      requiredDuringInsert: true,
      defaultConstraints: GeneratedColumn.constraintIsAlways(
          'REFERENCES kind_entries (id) ON DELETE CASCADE'));
  static const VerificationMeta _logMeta = const VerificationMeta('log');
  @override
  late final GeneratedColumn<int> log = GeneratedColumn<int>(
      'log', aliasedName, false,
      type: DriftSqlType.int,
      requiredDuringInsert: true,
      defaultConstraints: GeneratedColumn.constraintIsAlways(
          'REFERENCES logs (id) ON DELETE CASCADE'));
  @override
  List<GeneratedColumn> get $columns => [id, entry, kindEntry, log];
  @override
  String get aliasedName => _alias ?? 'color_entries';
  @override
  String get actualTableName => 'color_entries';
  @override
  VerificationContext validateIntegrity(Insertable<ColorEntry> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('entry')) {
      context.handle(
          _entryMeta, entry.isAcceptableOrUnknown(data['entry']!, _entryMeta));
    } else if (isInserting) {
      context.missing(_entryMeta);
    }
    if (data.containsKey('kind_entry')) {
      context.handle(_kindEntryMeta,
          kindEntry.isAcceptableOrUnknown(data['kind_entry']!, _kindEntryMeta));
    } else if (isInserting) {
      context.missing(_kindEntryMeta);
    }
    if (data.containsKey('log')) {
      context.handle(
          _logMeta, log.isAcceptableOrUnknown(data['log']!, _logMeta));
    } else if (isInserting) {
      context.missing(_logMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  ColorEntry map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return ColorEntry(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      entry: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}entry'])!,
      kindEntry: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}kind_entry'])!,
      log: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}log'])!,
    );
  }

  @override
  $ColorEntriesTable createAlias(String alias) {
    return $ColorEntriesTable(attachedDatabase, alias);
  }
}

class ColorEntry extends DataClass implements Insertable<ColorEntry> {
  final int id;
  final int entry;
  final int kindEntry;
  final int log;
  const ColorEntry(
      {required this.id,
      required this.entry,
      required this.kindEntry,
      required this.log});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['entry'] = Variable<int>(entry);
    map['kind_entry'] = Variable<int>(kindEntry);
    map['log'] = Variable<int>(log);
    return map;
  }

  ColorEntriesCompanion toCompanion(bool nullToAbsent) {
    return ColorEntriesCompanion(
      id: Value(id),
      entry: Value(entry),
      kindEntry: Value(kindEntry),
      log: Value(log),
    );
  }

  factory ColorEntry.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return ColorEntry(
      id: serializer.fromJson<int>(json['id']),
      entry: serializer.fromJson<int>(json['entry']),
      kindEntry: serializer.fromJson<int>(json['kindEntry']),
      log: serializer.fromJson<int>(json['log']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'entry': serializer.toJson<int>(entry),
      'kindEntry': serializer.toJson<int>(kindEntry),
      'log': serializer.toJson<int>(log),
    };
  }

  ColorEntry copyWith({int? id, int? entry, int? kindEntry, int? log}) =>
      ColorEntry(
        id: id ?? this.id,
        entry: entry ?? this.entry,
        kindEntry: kindEntry ?? this.kindEntry,
        log: log ?? this.log,
      );
  @override
  String toString() {
    return (StringBuffer('ColorEntry(')
          ..write('id: $id, ')
          ..write('entry: $entry, ')
          ..write('kindEntry: $kindEntry, ')
          ..write('log: $log')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, entry, kindEntry, log);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is ColorEntry &&
          other.id == this.id &&
          other.entry == this.entry &&
          other.kindEntry == this.kindEntry &&
          other.log == this.log);
}

class ColorEntriesCompanion extends UpdateCompanion<ColorEntry> {
  final Value<int> id;
  final Value<int> entry;
  final Value<int> kindEntry;
  final Value<int> log;
  const ColorEntriesCompanion({
    this.id = const Value.absent(),
    this.entry = const Value.absent(),
    this.kindEntry = const Value.absent(),
    this.log = const Value.absent(),
  });
  ColorEntriesCompanion.insert({
    this.id = const Value.absent(),
    required int entry,
    required int kindEntry,
    required int log,
  })  : entry = Value(entry),
        kindEntry = Value(kindEntry),
        log = Value(log);
  static Insertable<ColorEntry> custom({
    Expression<int>? id,
    Expression<int>? entry,
    Expression<int>? kindEntry,
    Expression<int>? log,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (entry != null) 'entry': entry,
      if (kindEntry != null) 'kind_entry': kindEntry,
      if (log != null) 'log': log,
    });
  }

  ColorEntriesCompanion copyWith(
      {Value<int>? id,
      Value<int>? entry,
      Value<int>? kindEntry,
      Value<int>? log}) {
    return ColorEntriesCompanion(
      id: id ?? this.id,
      entry: entry ?? this.entry,
      kindEntry: kindEntry ?? this.kindEntry,
      log: log ?? this.log,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (entry.present) {
      map['entry'] = Variable<int>(entry.value);
    }
    if (kindEntry.present) {
      map['kind_entry'] = Variable<int>(kindEntry.value);
    }
    if (log.present) {
      map['log'] = Variable<int>(log.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('ColorEntriesCompanion(')
          ..write('id: $id, ')
          ..write('entry: $entry, ')
          ..write('kindEntry: $kindEntry, ')
          ..write('log: $log')
          ..write(')'))
        .toString();
  }
}

abstract class _$AppDatabase extends GeneratedDatabase {
  _$AppDatabase(QueryExecutor e) : super(e);
  late final $KindsTable kinds = $KindsTable(this);
  late final $LogsTable logs = $LogsTable(this);
  late final $KindEntriesTable kindEntries = $KindEntriesTable(this);
  late final $NumEntriesTable numEntries = $NumEntriesTable(this);
  late final $TextEntriesTable textEntries = $TextEntriesTable(this);
  late final $ColorEntriesTable colorEntries = $ColorEntriesTable(this);
  @override
  Iterable<TableInfo<Table, Object?>> get allTables =>
      allSchemaEntities.whereType<TableInfo<Table, Object?>>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities =>
      [kinds, logs, kindEntries, numEntries, textEntries, colorEntries];
  @override
  StreamQueryUpdateRules get streamUpdateRules => const StreamQueryUpdateRules(
        [
          WritePropagation(
            on: TableUpdateQuery.onTableName('kinds',
                limitUpdateKind: UpdateKind.delete),
            result: [
              TableUpdate('logs', kind: UpdateKind.delete),
            ],
          ),
          WritePropagation(
            on: TableUpdateQuery.onTableName('kinds',
                limitUpdateKind: UpdateKind.delete),
            result: [
              TableUpdate('kind_entries', kind: UpdateKind.delete),
            ],
          ),
          WritePropagation(
            on: TableUpdateQuery.onTableName('kind_entries',
                limitUpdateKind: UpdateKind.delete),
            result: [
              TableUpdate('num_entries', kind: UpdateKind.delete),
            ],
          ),
          WritePropagation(
            on: TableUpdateQuery.onTableName('logs',
                limitUpdateKind: UpdateKind.delete),
            result: [
              TableUpdate('num_entries', kind: UpdateKind.delete),
            ],
          ),
          WritePropagation(
            on: TableUpdateQuery.onTableName('kind_entries',
                limitUpdateKind: UpdateKind.delete),
            result: [
              TableUpdate('text_entries', kind: UpdateKind.delete),
            ],
          ),
          WritePropagation(
            on: TableUpdateQuery.onTableName('logs',
                limitUpdateKind: UpdateKind.delete),
            result: [
              TableUpdate('text_entries', kind: UpdateKind.delete),
            ],
          ),
          WritePropagation(
            on: TableUpdateQuery.onTableName('kind_entries',
                limitUpdateKind: UpdateKind.delete),
            result: [
              TableUpdate('color_entries', kind: UpdateKind.delete),
            ],
          ),
          WritePropagation(
            on: TableUpdateQuery.onTableName('logs',
                limitUpdateKind: UpdateKind.delete),
            result: [
              TableUpdate('color_entries', kind: UpdateKind.delete),
            ],
          ),
        ],
      );
}
