import 'package:drift/drift.dart';

class Logs extends Table {
  IntColumn get id => integer().autoIncrement()();
  DateTimeColumn get createdAt => dateTime().withDefault(currentDateAndTime)();
  // we don't want dangling logs, that would be pretty bad
  IntColumn get kind =>
      integer().references(Kinds, #id, onDelete: KeyAction.cascade)();
}

class Kinds extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get name => text()();
}

enum Types {
  number,
  text,
  color,
}

@DataClassName("KindEntry")
class KindEntries extends Table {
  IntColumn get id => integer().autoIncrement()();
  // we don't want dangling entries
  IntColumn get kind =>
      integer().references(Kinds, #id, onDelete: KeyAction.cascade)();
  IntColumn get type => intEnum<Types>()();
  TextColumn get name => text()();
}

@DataClassName("NumEntry")
class NumEntries extends Table {
  IntColumn get id => integer().autoIncrement()();

  RealColumn get entry => real()();

  // we don't want dangling entries
  IntColumn get kindEntry =>
      integer().references(KindEntries, #id, onDelete: KeyAction.cascade)();
  IntColumn get log =>
      integer().references(Logs, #id, onDelete: KeyAction.cascade)();
}

@DataClassName("TextEntry")
class TextEntries extends Table {
  IntColumn get id => integer().autoIncrement()();

  TextColumn get entry => text()();

  // we don't want dangling entries
  IntColumn get kindEntry =>
      integer().references(KindEntries, #id, onDelete: KeyAction.cascade)();
  IntColumn get log =>
      integer().references(Logs, #id, onDelete: KeyAction.cascade)();
}

@DataClassName("ColorEntry")
class ColorEntries extends Table {
  IntColumn get id => integer().autoIncrement()();

  IntColumn get entry => integer()();

  // we don't want dangling entries
  IntColumn get kindEntry =>
      integer().references(KindEntries, #id, onDelete: KeyAction.cascade)();
  IntColumn get log =>
      integer().references(Logs, #id, onDelete: KeyAction.cascade)();
}
