import 'package:crap_manager/db/definition.dart';
import 'package:crap_manager/db/tables.dart';

class EntryWithKind {
  EntryWithKind(this.entry, this.kind);

  final KindEntry entry;
  final Kind kind;
}

class KindEntryCreateData {
  String name;
  Types type;

  KindEntryCreateData(this.name, this.type);

  @override
  String toString() {
    return "KindEntryCreateData(name: \"$name\", type: $type)";
  }
}

class KindEntryCreateDataNullable {
  String? name;
  Types? type;

  KindEntryCreateDataNullable(this.name, this.type);

  @override
  String toString() {
    return "KindEntryCreateDataNullable(name: \"$name\", type: $type)";
  }
}

class NumEntryWithKindEntry {
  NumEntryWithKindEntry(this.numEntry, this.kindEntry);

  final NumEntry numEntry;
  final KindEntry kindEntry;
}

class TextEntryWithKindEntry {
  TextEntryWithKindEntry(this.textEntry, this.kindEntry);

  final TextEntry textEntry;
  final KindEntry kindEntry;
}

class ColorEntryWithKindEntry {
  ColorEntryWithKindEntry(this.colorEntry, this.kindEntry);

  final ColorEntry colorEntry;
  final KindEntry kindEntry;
}

class LogWithFullinfo {
  LogWithFullinfo(this.log, this.kind, this.numEntries, this.textEntries,
      this.colorEntries);

  final Log log;
  final Kind kind;
  final List<NumEntryWithKindEntry> numEntries;
  final List<TextEntryWithKindEntry> textEntries;
  final List<ColorEntryWithKindEntry> colorEntries;
}

class KindWithKindEntries {
  KindWithKindEntries(this.kind, this.kindEntries);

  final Kind kind;
  final List<KindEntry> kindEntries;

  @override
  int get hashCode => kind.id;

  @override
  bool operator ==(Object other) {
    if (other is! KindWithKindEntries) {
      return false;
    }
    return kind.id == other.kind.id;
  }
}
