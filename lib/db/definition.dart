import "dart:io";
import 'dart:ui';
import "package:drift/drift.dart";
import 'package:drift/native.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' as p;

import 'package:crap_manager/db/helper_types.dart';
import 'package:crap_manager/db/tables.dart';

part "definition.g.dart";

final AppDatabase db = AppDatabase();

@DriftDatabase(
    tables: [Logs, Kinds, KindEntries, NumEntries, TextEntries, ColorEntries])
class AppDatabase extends _$AppDatabase {
  AppDatabase() : super(_openConnection());

  Future<List<EntryWithKind>> entriesWithKind(Kind kind) async {
    final result = await (select(kindEntries)
          ..where((tbl) => tbl.kind.equals(kind.id)))
        .join(
            [leftOuterJoin(kinds, kinds.id.equalsExp(kindEntries.kind))]).get();
    return result
        .map((row) =>
            EntryWithKind(row.readTable(kindEntries), row.readTable(kinds)))
        .toList();
  }

  Future<Kind?> getKindWithName(String name) async {
    return (select(kinds)..where((tbl) => tbl.name.equals(name)))
        .getSingleOrNull();
  }

  Future<void> insertKindWithEntries(
      String kindName, List<KindEntryCreateData> entriesData) async {
    transaction(() async {
      final kind = await getOrInsertKindWithName(kindName);
      for (final data in entriesData) {
        await getOrAddEntryToKind(kind.id, data.name, data.type);
      }
    });
  }

  Future<KindEntry> getOrAddEntryToKind(
      int kindId, String name, Types type) async {
    var result = await (select(kindEntries)
          ..where((tbl) =>
              tbl.name.equals(name) &
              tbl.kind.equals(kindId) &
              tbl.type.equals(type.index)))
        .getSingleOrNull();
    if (result == null) {
      await into(kindEntries).insert(
          KindEntriesCompanion.insert(kind: kindId, name: name, type: type));
      result = await (select(kindEntries)
            ..where((tbl) =>
                tbl.name.equals(name) &
                tbl.kind.equals(kindId) &
                tbl.type.equals(type.index)))
          .getSingle();
    }
    return result;
  }

  Future<Kind> getOrInsertKindWithName(String name) async {
    var result = await (select(kinds)..where((tbl) => tbl.name.equals(name)))
        .getSingleOrNull();
    if (result == null) {
      await into(kinds).insert(KindsCompanion.insert(name: name));
      result = await (select(kinds)..where((tbl) => tbl.name.equals(name)))
          .getSingle();
    }
    return result;
  }

  Future<void> insertLog(DateTime createdAt, Kind kind,
      Map<KindEntry, dynamic> entriesData) async {
    transaction(() async {
      final logId = await into(logs).insert(
          LogsCompanion.insert(createdAt: Value(createdAt), kind: kind.id));

      for (final entry in entriesData.keys) {
        if (entry.type == Types.number) {
          assert(entriesData[entry] is double);
          await into(numEntries).insert(NumEntriesCompanion.insert(
              entry: entriesData[entry], kindEntry: entry.id, log: logId));
        } else if (entry.type == Types.text) {
          assert(entriesData[entry] is String);
          await into(textEntries).insert(TextEntriesCompanion.insert(
              entry: entriesData[entry], kindEntry: entry.id, log: logId));
        } else if (entry.type == Types.color) {
          assert(entriesData[entry] is Color);
          await into(colorEntries).insert(ColorEntriesCompanion.insert(
              entry: entriesData[entry].value,
              kindEntry: entry.id,
              log: logId));
        }
      }
    });
  }

  Future<List<NumEntryWithKindEntry>> getNumEntriesWithKindEntry(
      Log log) async {
    final res = await (select(numEntries).join([
      innerJoin(kindEntries, numEntries.kindEntry.equalsExp(kindEntries.id))
    ])
          ..where(numEntries.log.equals(log.id))
          ..orderBy([OrderingTerm(expression: kindEntries.id)]))
        .get();
    return res
        .map((row) => NumEntryWithKindEntry(
            row.readTable(numEntries), row.readTable(kindEntries)))
        .toList();
  }

  Future<List<TextEntryWithKindEntry>> getTextEntriesWithKindEntry(
      Log log) async {
    final res = await (select(textEntries).join([
      innerJoin(kindEntries, textEntries.kindEntry.equalsExp(kindEntries.id))
    ])
          ..where(textEntries.log.equals(log.id))
          ..orderBy([OrderingTerm(expression: kindEntries.id)]))
        .get();
    return res
        .map((row) => TextEntryWithKindEntry(
            row.readTable(textEntries), row.readTable(kindEntries)))
        .toList();
  }

  Future<List<ColorEntryWithKindEntry>> getColorEntriesWithKindEntry(
      Log log) async {
    final res = await (select(colorEntries).join([
      innerJoin(kindEntries, colorEntries.kindEntry.equalsExp(kindEntries.id))
    ])
          ..where(colorEntries.log.equals(log.id))
          ..orderBy([OrderingTerm(expression: kindEntries.id)]))
        .get();
    return res
        .map((row) => ColorEntryWithKindEntry(
            row.readTable(colorEntries), row.readTable(kindEntries)))
        .toList();
  }

  Future<KindWithKindEntries> getKindWithKindEntries(Kind kind) async {
    final res = await (select(kindEntries)
          ..where((tbl) => tbl.kind.equals(kind.id))
          ..orderBy([
            (u) => OrderingTerm(expression: u.type),
            (u) => OrderingTerm(expression: u.id),
          ]))
        .get();
    final kwke = KindWithKindEntries(kind, []);
    for (final row in res) {
      kwke.kindEntries.add(row);
    }
    return kwke;
  }

  Future<List<LogWithFullinfo>> getLogsWithEntries() async {
    final allLogs = await select(logs).get();
    final data = <LogWithFullinfo>[];
    for (final log in allLogs) {
      final numEntries = await getNumEntriesWithKindEntry(log);
      final textEntries = await getTextEntriesWithKindEntry(log);
      final colorEntries = await getColorEntriesWithKindEntry(log);
      final kind = await (select(kinds)
            ..where((tbl) => tbl.id.equals(log.kind)))
          .getSingle();
      data.add(
          LogWithFullinfo(log, kind, numEntries, textEntries, colorEntries));
    }

    return data;
  }

  Stream<List<LogWithFullinfo>> watchLogsWithEntries() async* {
    final logsWatch = select(logs).watch();
    await for (final allLogs in logsWatch) {
      final data = <LogWithFullinfo>[];
      for (final log in allLogs) {
        final numEntries = await getNumEntriesWithKindEntry(log);
        final textEntries = await getTextEntriesWithKindEntry(log);
        final colorEntries = await getColorEntriesWithKindEntry(log);
        final kind = await (select(kinds)
              ..where((tbl) => tbl.id.equals(log.kind)))
            .getSingle();
        data.add(
            LogWithFullinfo(log, kind, numEntries, textEntries, colorEntries));
      }

      yield data;
    }
  }

  Stream<Map<Kind, List<LogWithFullinfo>>> watchLogsByKind() async* {
    await for (final allLogs in watchLogsWithEntries()) {
      Map<Kind, List<LogWithFullinfo>> map = {};
      for (final log in allLogs) {
        if (map[log.kind] == null) map[log.kind] = [];
        map[log.kind]!.add(log);
      }
      yield map;
    }
  }

  Future<void> deleteEverything() async {
    // this should wipe the database, since all our
    // tables depend on `kinds` either directly or indirectly.
    await delete(kinds).go();
  }

  Stream<Map<KindWithKindEntries, List<LogWithFullinfo>>>
      watchKindsAndLogs() async* {
    final query = select(kinds)
        .join([leftOuterJoin(logs, logs.kind.equalsExp(kinds.id))]).watch();

    await for (final kindsRes in query) {
      Map<KindWithKindEntries, List<LogWithFullinfo>> kindsToLogs = {};
      for (final res in kindsRes) {
        final kind = res.readTable(kinds);
        // TODO: Ensure that KindEntries update when necessary
        final kindEntries = await getKindWithKindEntries(kind);
        if (kindsToLogs[kindEntries] == null) {
          kindsToLogs[kindEntries] = [];
        }
        final log = res.readTableOrNull(logs);
        if (log == null) {
          continue;
        }
        final numEntries = await getNumEntriesWithKindEntry(log);
        final textEntries = await getTextEntriesWithKindEntry(log);
        final colorEntries = await getColorEntriesWithKindEntry(log);
        kindsToLogs[kindEntries]!.add(
            LogWithFullinfo(log, kind, numEntries, textEntries, colorEntries));
      }
      yield kindsToLogs;
    }
  }

  @override
  int get schemaVersion => 1;

  @override
  MigrationStrategy get migration {
    return MigrationStrategy(beforeOpen: (details) async {
      // this ensures that all our foreign key are valid
      // before we could delete stuff without checking if
      // anything else referenced it.
      await customStatement('PRAGMA foreign_keys = ON');
    });
  }
}

Future<String> databasePath() async {
  final dbFolder = await getApplicationDocumentsDirectory();
  return p.join(dbFolder.path, "crap_manager_data", 'db.sqlite');
}

LazyDatabase _openConnection() {
  return LazyDatabase(() async {
    final file = File(await databasePath());

    return NativeDatabase.createInBackground(file);
  });
}
