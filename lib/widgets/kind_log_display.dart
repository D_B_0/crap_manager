import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'package:crap_manager/db/helper_types.dart';

class KindLogDisplayWidget extends StatelessWidget {
  const KindLogDisplayWidget(this.kind, this.logs, {super.key});

  final KindWithKindEntries kind;
  final List<LogWithFullinfo> logs;

  List<DataColumn> _buildHeader() {
    return [
      const DataColumn(label: Expanded(child: Text("Date"))),
      for (final kindEntry in kind.kindEntries)
        // TODO: maybe also explicitly say the tipe of this entry?
        DataColumn(label: Expanded(child: Text(kindEntry.name))),
    ];
  }

  List<DataRow> _buildBody(BuildContext context) {
    if (logs.isEmpty) {
      // TODO: figure out something better for this
      return [_buildEmptyRow()];
    } else {
      return [
        for (final log in logs) _buildRow(context, log),
      ];
    }
  }

  DataRow _buildEmptyRow() {
    return DataRow(cells: [
      const DataCell(Text("")),
      for (int i = 0; i < kind.kindEntries.length; ++i)
        const DataCell(Text("")),
    ]);
  }

  Widget _dateTime(BuildContext context, LogWithFullinfo logInfo) {
    final date = AppLocalizations.of(context)!.createdAt(logInfo.log.createdAt);
    return Text(date);
  }

  Widget _num(NumEntryWithKindEntry entry) {
    return Text("${entry.numEntry.entry}");
  }

  Widget _text(TextEntryWithKindEntry entry) {
    return Text(entry.textEntry.entry);
  }

  Widget _color(ColorEntryWithKindEntry entry) {
    final color = Color(entry.colorEntry.entry);
    return DecoratedBox(
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(50),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 6, vertical: 2),
        child: Text("rgb(${color.red}, ${color.green}, ${color.blue})"),
      ),
    );
  }

  DataRow _buildRow(BuildContext context, LogWithFullinfo logInfo) {
    return DataRow(cells: [
      DataCell(_dateTime(context, logInfo)),
      for (final entry in logInfo.numEntries) DataCell(_num(entry)),
      for (final entry in logInfo.textEntries) DataCell(_text(entry)),
      for (final entry in logInfo.colorEntries) DataCell(_color(entry)),
    ]);
  }

  @override
  Widget build(BuildContext context) {
    return ExpansionTile(
      title: Text(kind.kind.name),
      children: [
        // TODO: This currently does not work, figure out scrolling
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: DataTable(
            columns: _buildHeader(),
            rows: _buildBody(context),
          ),
        ),
      ],
    );
  }
}
